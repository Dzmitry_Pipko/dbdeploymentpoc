﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Configuration;
using System.Reflection;

namespace DbUp
{
    class Program
    {
        static void Main(string[] args)
        {

            var connectionString = ConfigurationManager.ConnectionStrings["custom"].ConnectionString;

            var upgrader =
             DeployChanges.To
                 .SqlDatabase(connectionString)
                 .WithScriptsEmbeddedInAssembly(Assembly.GetExecutingAssembly(), it => it.Contains(".Script."))
                 .WithTransaction()
                 .LogToConsole()
                 .Build();

            var result = upgrader.PerformUpgrade();

            if (!result.Successful)
            {
                //do something
                return;
            }

        }
    }
}
